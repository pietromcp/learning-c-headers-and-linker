#include <foobar.h>
#include <mymath.h>

int foobar(int x, int y) {
	return sum(x, x) + mul(y, y);
}
