#!/bin/bash
rm libs/*
echo "Compiling libmymath.so..."
gcc -o libs/libmymath.so -fPIC -shared -Iheaders impl/mymath.c || exit 1
echo "Compiling libfoobar.so..."
gcc impl/foobar.c -shared -fPIC -o libs/libfoobar.so -Iheaders -Llibs -lmymath -Wl,-rpath,'$ORIGIN/.' || exit 2
echo "Compiling main..."
gcc impl/main.c -Iheaders  -o main -Llibs -lmymath -lfoobar -Wl,-rpath,'$ORIGIN/libs' || exit 3
